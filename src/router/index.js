import { createRouter, createWebHistory } from "vue-router";
import Home from "../views/Home.vue";



import NavbarLogged from "../components/Navbar.vue";
import Users from '../components/Users.vue';

const routes = [
  {
    path: "/",
    name: "Home",
    component: Home
  },
  {
    path: "/logged",
    name: "NavbarLogged",
    component: NavbarLogged
  },
  {
    path: "/users",
    name: "Users",
    component: Users
  },
  
  
 


 
];

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
});

export default router;
